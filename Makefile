CC=gcc
CXX=g++
RM=rm -f
CPPFLAGS=-g --std=c++11 -fopenmp
LDFLAGS=-g
LDLIBS=

APP=Parser_v1_2_2.exe

SRCS=parser.cpp
OBJS=$(subst .cc,.o,$(SRCS))

all: $(APP)

$(APP): $(OBJS)
	$(CXX) $(LDFLAGS) -o $(APP) $(OBJS) $(LDLIBS) $(CPPFLAGS)

clean:
	$(RM) *.o

#run: Parser.exe ..\medrevhigh_5.pdp 10

dist-clean: clean
	$(RM) $(APP)