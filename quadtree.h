#ifndef _QUADTREE_H_
#define _QUADTREE_H_

#include <vector>
#include <iostream>
#include <ostream>
#include <sstream>
#include <algorithm>
#include <cmath>

using namespace std;

/* * * * * * *
    POINT
* * * * * * */
struct Point {
    float x = 0.0f;
    float y = 0.0f;
    Point (float x, float y) : x(x), y(y) {};
    Point () : x(0.0f), y(0.0f) {};
};


/* * * * * * *
    AABB
* * * * * * */
struct AABB {
    Point centre;
    Point halfSize;

    AABB(Point centre, Point halfSize): centre(centre), halfSize(halfSize) {};

    // Given a Point coordinate, checks to see if it is contained within this AABB
    bool contains(const Point & a) const {
        if(a.x < centre.x + halfSize.x && a.x > centre.x - halfSize.y) {
            if(a.y < centre.y + halfSize.x && a.y > centre.y - halfSize.y) {
                return true;
            }
        }
        return false;
    }

    // Checks for intersection between two AABB
    bool intersects(const AABB & other) const {
        //this right > that left                                          this left <s that right
        if (centre.x + halfSize.x > other.centre.x - other.halfSize.x || centre.x - halfSize.x < other.centre.x + other.halfSize.x) {
        // This bottom > that top
            if (centre.y + halfSize.y > other.centre.y - other.halfSize.y || centre.y - halfSize.y < other.centre.y + other.halfSize.y) {
                return true;
            }
        }
        return false;
    }
};

/* * * * * * *
    DATA
* * * * * * */
template <typename T>
struct Data
{
    Point pos;
    T* load;

    Data(Point pos = Point(), T* data = NULL): pos(pos), load(data){};
};

/* * * * * * * *
    QUADTREE
* * * * * * * */
template <class T>
class Quadtree {
    public:
        //4 children
        Quadtree* nw;
        Quadtree* ne;
        Quadtree* sw;
        Quadtree* se;

        AABB boundary;

        vector< Data<T> > objects;
        vector< Data<T> > clusteredObjects;

        bool optimizing;
        Quadtree* lookupTree;
        int maxObjectsHeldInNode = 1000,
            maxClusteredObjectsHeldInNode = 100;

        Quadtree<T>();
        Quadtree<T>(AABB boundary);
        Quadtree<T>(AABB boundary, Quadtree<T>* lookupTree);

        ~Quadtree();

        friend ostream& operator<<(ostream& os, const Quadtree &qt) {
            int i;
            // Write the bounding box
            os  << qt.boundary.centre.x << " "
                << qt.boundary.centre.y << " "
                << qt.boundary.halfSize.x << " "
                << qt.boundary.halfSize.y << " ";
            // Write length of data
            os  << qt.objects.size() << " ";
            nodeCount++;
            // Write data
            for (i = 0; i < qt.objects.size(); i++) {
                plantSaveCount++;
                os  << qt.objects.at(i).load->x << " "
                    << qt.objects.at(i).load->y << " "
                    << qt.objects.at(i).load->z << " "
                    << qt.objects.at(i).load->species << " "
                    << qt.objects.at(i).load->height << " "
                    << qt.objects.at(i).load->canopy << " ";
            }
            // Write clustered objects
            os  << qt.clusteredObjects.size() << " ";
            for (i = 0; i < qt.clusteredObjects.size(); i++) {
                clusteredPlantSaveCount++;
                os  << qt.clusteredObjects.at(i).load->x << " "
                    << qt.clusteredObjects.at(i).load->y << " "
                    << qt.clusteredObjects.at(i).load->z << " "
                    << qt.clusteredObjects.at(i).load->species << " "
                    << qt.clusteredObjects.at(i).load->height << " "
                    << qt.clusteredObjects.at(i).load->canopy << " ";
            }

            if (qt.nw == NULL)
               os << ";";

            os << endl;

            return os;
        }

        bool insert(Data<T> d, bool isClusteredData);
        void subdivide();
        vector< Data<T> > queryRange(AABB range);
};

template <class T>
Quadtree<T>::Quadtree() :
    nw(nullptr),
    ne(nullptr),
    sw(nullptr),
    se(nullptr),
    optimizing(false),
    lookupTree(NULL)
{
    boundary = AABB();
    objects = vector< Data<T> >();
}

template <class T>
Quadtree<T>::Quadtree(AABB boundary) :
    nw(nullptr),
    ne(nullptr),
    sw(nullptr),
    se(nullptr),
    boundary(boundary),
    optimizing(false),
    lookupTree(NULL)
{
    objects = vector< Data<T> >();
}

template <class T>
Quadtree<T>::Quadtree(AABB boundary, Quadtree<T>* lookupTree) :
    nw(nullptr),
    ne(nullptr),
    sw(nullptr),
    se(nullptr),
    boundary(boundary),
    optimizing(true),
    lookupTree(lookupTree)
{
    objects = vector< Data<T> >();
}

template <class T>
Quadtree<T>::~Quadtree()
{
    delete nw;
    delete sw;
    delete ne;
    delete se;
}

template <class T>
void Quadtree<T>::subdivide()
{
    // If the tree is being balanced, split the quadrants according to the best
    // selected point
    if (optimizing) {
        //vector<Data <T>> range = lookupTree->queryRange(boundary);
        //qCentre = getBestPoint(range.size(), range);
    } else {
        // The new quadrants are a quater of the size of the current boundary
        Point quadSize(boundary.halfSize.x/2, boundary.halfSize.y/2);

        // North West Quadrant
        Point nwCenter = Point(boundary.centre.x - quadSize.x, boundary.centre.y + quadSize.y);
        nw = new Quadtree(AABB(nwCenter, quadSize));
        // North East Quadrant
        Point neCenter = Point(boundary.centre.x + quadSize.x, boundary.centre.y + quadSize.y);
        ne = new Quadtree(AABB(neCenter, quadSize));
        // South West Quadrant
        Point swCenter = Point(boundary.centre.x - quadSize.x, boundary.centre.y - quadSize.y);
        sw = new Quadtree(AABB(swCenter, quadSize));
        // South East Quadrant
        Point seCenter = Point(boundary.centre.x + quadSize.x, boundary.centre.y - quadSize.y);
        se = new Quadtree(AABB(seCenter, quadSize));
    }   
}


template <class T>
bool Quadtree<T>::insert(Data<T> d, bool isClusteredData) {
    // Ensure that it is inside the root boundary
    if(!boundary.contains(d.pos)) {
        return false;
    }

    // Add to the leaf node if it has space inside of its object list
    if (isClusteredData) {
        if(clusteredObjects.size() < maxClusteredObjectsHeldInNode) {
            clusteredObjects.push_back(d);
            return true;
        }
    } else {
        if(objects.size() < maxObjectsHeldInNode) {
            objects.push_back(d);
            return true;
        }
    }

    // If this leaf node doesn't have space and there are no child nodes, generate
    // child nodes and insert
    if(nw == NULL) subdivide();

    // Do the insertion
    if(nw->insert(d, isClusteredData)) return true;
    if(ne->insert(d, isClusteredData)) return true;
    if(sw->insert(d, isClusteredData)) return true;
    if(se->insert(d, isClusteredData)) return true;

    return false;   
}

template <class T>
vector< Data<T> > Quadtree<T>::queryRange(AABB range)
{
    vector< Data<T> > pInRange = vector< Data<T> >();

    if(!boundary.intersects(range))
    {
        return pInRange;
    }

    for(int i = 0; i < objects.size(); i++)
    {
        if(range.contains(objects.at(i).pos))
        {
            pInRange.push_back(objects.at(i));
        }
    }

    if(nw == NULL)
    {
        return pInRange;
   }

    vector< Data<T> > temp = nw->queryRange(range);
    pInRange.insert(pInRange.end(), temp.begin(), temp.end());

    temp = ne->queryRange(range);
    pInRange.insert(pInRange.end(), temp.begin(), temp.end());

    temp = sw->queryRange(range);
    pInRange.insert(pInRange.end(), temp.begin(), temp.end());

    temp = se->queryRange(range);
    pInRange.insert(pInRange.end(), temp.begin(), temp.end());

    return pInRange;
}


/* * * * * * * * * * *
    HELPER METHODS
* * * * * * * * * * */

template <class T>
int getDepth(Quadtree<T>& node) {
    int deepest = 0;

    if (node.nw != NULL) {
        deepest = max(deepest, getDepth(*node.nw));
        deepest = max(deepest, getDepth(*node.ne));
        deepest = max(deepest, getDepth(*node.sw));
        deepest = max(deepest, getDepth(*node.se));    
    }

    return deepest + 1;
}

template <class T>
Point virtualPointFinder(int size, vector<Data<T>> quad) {
    vector<Data<T> *> plantsSortedInX;
    vector<Data<T> *> plantsSortedInY;

    // Copy pointers to plants array objects to both vectors
    for (int i = 0; i < size; i++) {
        plantsSortedInY.push_back(&quad.at(i));
        plantsSortedInX.push_back(&quad.at(i));
    }

    // Sort both vectors accordingly
    sort(plantsSortedInX.begin(), plantsSortedInX.end(), [](Data<T> * a, Data<T> * b) {
        return (a->pos.x < b->pos.x);
    });
    sort(plantsSortedInY.begin(), plantsSortedInY.end(), [](Data<T> * a, Data<T> * b) {
        return (a->pos.y < b->pos.y);
    });

    // Generate a virtual point list using the xValue from the ascending X value sorted list
    // and the yValue from the ascending Y value sorted list.
    Point storedVirtualPoint;
    float storedVTMR = numeric_limits<float>::max();

    for (int i = 0; i < size; i += 100) {
        int nwCount = 0,
            neCount = 0,
            swCount = 0,
            seCount = 0;
        Point virtualPoint(plantsSortedInX.at(i)->pos.x, plantsSortedInY.at(i)->pos.y);

        // Count the number of points in either quadrant
        //#pragma omp parallel for
        for (int k = 0; k < size; k++) {
            Point p = quad.at(k).pos;
            // North West Quadrant
            if (p.x <= virtualPoint.x && p.y >= virtualPoint.y) nwCount++;
            // North East Quadrant
            if (p.x >= virtualPoint.x && p.y >= virtualPoint.y) neCount++;
            // South West Quadrant
            if (p.x <= virtualPoint.x && p.y <= virtualPoint.y) swCount++;
            // South East Quadrant
            if (p.x >= virtualPoint.x && p.y <= virtualPoint.y) seCount++;
        }
        float n = 4;
        float v = ((pow(nwCount, 2) + pow(neCount, 2) + pow(swCount, 2) + pow(seCount, 2)) - (pow(size, 2) / n)) / (n - 1);
        float m = size / n;
        float vtmr = v / m;

        if (vtmr < storedVTMR) {
            storedVTMR = vtmr;
            storedVirtualPoint = virtualPoint;
        }

        //cout << nwCount << " " << neCount << " " << swCount << " " << seCount << " VTMR: " << vtmr << " Stored VTMR: " << storedVTMR << endl;
    }

    return storedVirtualPoint;
}

template <class T>
Point physicalPointFinder(int size, vector<Data<T>> quad, Point vp) {
    Point storedPoint;
    float distance = numeric_limits<float>::max();

    for (int i = 0; i < size; i++) {
        Point p = quad.at(i).pos;
        float val = pow(vp.x - p.x, 2) + pow(vp.y - p.y, 2);
        if (val < distance) {
            storedPoint = p;
            distance = val;
        }
    }
    //cout << "X: " << vp.x << " Y: " << vp.y << endl;
    //cout << "X: " << storedPoint.x << " Y: " << storedPoint.y << endl;

    return storedPoint;
}

template <class T>
Point getBestPoint(int size, vector<Data<T>> quad) {
    Point physicalPoint;
    if (!quad.empty()) {
        Point virtualPoint = virtualPointFinder(size, quad); // to find out seed points
        physicalPoint = physicalPointFinder(size, quad, virtualPoint); // to find out physical points
    }
    return physicalPoint;
}

template <class T>
bool isBalanced(Quadtree<T> *root)
{
    if (root == NULL || root->nw == NULL)
        return true;
    else {
        int nw = getDepth(*root->nw),
            ne = getDepth(*root->ne),
            sw = getDepth(*root->sw),
            se = getDepth(*root->se);

        return (isBalanced(root->nw) && isBalanced(root->ne) && isBalanced(root->sw) && isBalanced(root->se) &&
            (abs(nw - ne) <= 1 && abs(nw - sw) <= 1 && abs(nw - se) <= 1 &&
            abs(ne - sw) <= 1 && abs(ne - se) <= 1 && abs(sw - se) <= 1));
    }
}


#endif