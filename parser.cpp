#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <math.h>
#include <vector>
#include <string.h>
#include <queue>
#include <limits>
#include "terragen.h"
#include "grass.h"

// Global variables
int plantCount = 0,
    clusteredPlantSaveCount = 0,
    plantSaveCount = 0,
    nodeCount = 0,
    maxX = -10000000,
    maxY = maxX,
    viewersHeight = 2,  // meters
    maxsize = 1000000;  // meters or centimeters
bool meters = false;     // if false => centimeters is being used

#include "quadtree.h"
#include "map.h"


using namespace std;

// Plant structure
struct Plant {
	Plant(float x, float y, float z, int species, float height, float canopy) : x(x), y(y), z(z), species(species), height(height), canopy(canopy) {};

	int 	species;
	float 	height, canopy, x, y, z;
};

struct ClusteredPlant : Plant {
    ClusteredPlant(float x, float y, float z, int species, int averageHeight) : Plant(x, y, z, species, averageHeight, 0) {};
};

bool readPDP(string filename, int mapsize, Quadtree<Plant>& qt, vector<Data<Plant>>& plants) {
    ifstream 	infile;
    int 		numSpecies;
    float 		rndoff;

    // Open the file to be parsed and processed
    infile.open((char *) filename.c_str(), ios_base::in);
    if(infile.is_open()) {
        infile >> numSpecies;

        cout << "Number of species detected: " << numSpecies << endl;
        for(int i = 0; i < numSpecies; i++) { //numSpecies
            int specName, numPlants;
            float canopyRatio, hMin, hMax;
            // pdb units are in cm for both height and x, y location

            infile >> specName;

            cout << "* Starting on '" << specName << "' species" << endl;

            infile >> hMin >> hMax >> canopyRatio;
            
            infile >> numPlants;

            cout << "\tNumber of plants detected: " << numPlants << endl;
            
            for(int j = 0; j < numPlants; j++) { //numPlants
                float 	height, canopy, x, y, z;
                // terrain position and plant height
                infile >> x >> y >> z >> height;

                // Misc: Classify max x and y values for the .pdp dataset
                if (x > maxX) maxX = x;
                if (y > maxY) maxY = y;

                // Scale height accordingly
                
                if (meters) {
                    x = (x / (float)maxsize) * mapsize;
                    y = (y / (float)maxsize) * mapsize;
                   // height = (height / (float)maxsize) * mapsize;
                } else {
                    height /= 100.0f;
                    x = (x / (maxsize * 100.0f)) * mapsize;
                    y = (y / (maxsize * 100.0f)) * mapsize;
                }
                // supplied canopy ratio is actually radius to height (not diameter to height)
                canopy = height * canopyRatio * 2.0f; // probably worth adding some randomness as well here
                // Generate plant

                Plant *p = new Plant(x, y, z, specName, height, canopy);
                // Insert the plant into the quadtree and list of all plants
                Data<Plant> data(Point(x, y), p);

                qt.insert(data, false);
                plants.push_back(data);
                // Increment plant counter
                plantCount++;
            }
        }
        infile.close();
    }
    else
    {
        cout << "readPDP failed with: " << filename << endl;
        return false;
    }
    return true;
}

bool writePPDP(const string filename, Quadtree<Plant>* qt) {
    stringstream    ss;
    FILE            *file;
    float           mapsize = (float)qt->boundary.halfSize.x * 2;

    // Add the viewers scale
    ss << (viewersHeight / (float)maxsize) * mapsize << endl;

    // Output the quadtree to the .ppdp file using a queue with breadth first traversal
    queue<Quadtree<Plant>*> q;
    q.push(qt);

    while (!q.empty()) {
        Quadtree<Plant> * current = q.front();
        q.pop();
        if (current == NULL)
            continue;
        q.push(current->nw);
        q.push(current->ne);
        q.push(current->sw);
        q.push(current->se);

        ss << *current;
    }

    // Convert to char array    
    string s_out = ss.str();

    // Get the size
    ss.seekg(0, ios::end);
    int size = ss.tellg();
    ss.seekg(0, ios::beg);
    
    try {
        // Open file abd save as binary
        file = fopen(&filename[0], "wb");
        // Write to the file
        fwrite(s_out.c_str(), 1, size, file);
    } catch (exception & e) {
        cout << "ERR: " << e.what() << endl;
        return false;
    }

    fclose(file);

    return true;
}

void cluster(Quadtree<Plant>* qt, int width, int clusterWidth, int minPlantsRequired, int clusterStep) {
    cout << " * Clustering plants * " << endl;

    float   halfSize = width/2;
    int     maxClusteredPlants = minPlantsRequired,
            x, y, i, j,
            plantsContainedInRadius = 0,
            clusterHalfSize = clusterWidth/2;

    AABB aabb(Point(halfSize, halfSize), Point(halfSize, halfSize));    // Generate AABB for each grid cell
    vector<Data<Plant>> plants = qt->queryRange(aabb);  // Query AABB

    if (clusterStep < 1) clusterStep = 1;

    for (i = 0; i < plants.size(); i += clusterStep) {
        Data<Plant> plant = plants.at(i);
        if (plant.load->species != 4 && //grass
            plant.load->species != 202 && // mosscampione
            plant.load->species != 203) {  // daisy
            AABB clusterAABB(Point(plant.load->x, plant.load->y), Point(clusterHalfSize, clusterHalfSize));
            vector<Data<Plant>> clusteredPlants = qt->queryRange(clusterAABB);  // Query cluster AABB
            // Tally the number of plants for each species inside the clustered plants AABB
            if (clusteredPlants.size() > maxClusteredPlants) {
                for (j = 0; j < clusteredPlants.size(); j++) {
                    if (clusteredPlants.at(j).load->species == plant.load->species) plantsContainedInRadius++;
                }
            }
            // Granted there are enough plants in the region, continue to "cluster" them
            if (plantsContainedInRadius >= maxClusteredPlants) {
                // remove the retrieved clustered plants from the plants list for the grid cell
                // and generate a new "clustered plant"
                float tallestPlant = -100.0f;
                
                // Remove clustered plants from the plants list
                int index = 0;
                for (auto it = clusteredPlants.begin(); it != clusteredPlants.end(); ++it) {
                    if (index >= maxClusteredPlants) { break; }
                    if (it->load->species == plant.load->species) {
                        for (auto it_2 = plants.begin(); it_2 != plants.end(); ++it_2) {
                            if (it->pos.x == it_2->pos.x &&
                                it->pos.y == it_2->pos.y &&
                                it->load->species == it_2->load->species) {
                                plants.erase(it_2);
                                index++;
                                if (it->load->height > tallestPlant)
                                    tallestPlant = it->load->height;
                                break;
                            }
                        }
                    }
                }

                Plant *clusteredPlant = new ClusteredPlant(plant.load->x, plant.load->y, plant.load->z, plant.load->species, tallestPlant); // Create the clustered plant
                Data<Plant> data(Point(plant.load->x, plant.load->y), clusteredPlant);
                qt->insert(data, true); // Insert into the existing quadtree
                cout << plant.load->species << "(" << (int)(((float)i/(float)plants.size()) * 100) << "%) "; // Indicate a clustered plant has been completed
            }
            plantsContainedInRadius = 0;
        }
    }
}

int main(int args, char ** vargs) {
    vector<Data<Plant>> plants; // Vector of all plants
    int  height = 0,
         width = 512,
         minPlantsRequired = 10,
         clusterStep = 1;
    bool generateTree = false,
         generateMap = false,
         blur = false;
    string  pdp_filename,
            ter_filename,
            grass_filename;

    if (args > 2) {
        cout << "------ INPUTS/OUTPUTS ------" << endl;
        
        // file out prefix
        string filename_out = string(vargs[1]);
        string tree_filename = filename_out + ".tree",
               map_filename = filename_out + ".map";
        //cout << ".tree filename:\t" << tree_filename << endl
        //     << ".map filename:\t" << map_filename << endl;

        // Flag lookup
        // -m : meters (default = false)
        // -maxsize : maxsize (default = 1000000)
        // -cm : centimeters
        
            for (int i = 2; i < args; i++) {
                string flag = string(vargs[i]);    
                if (flag == "-meters") {
                    cout << "Unit of measure: meters" << endl;
                    meters = true;
                } else if (flag == "-cm") {
                    cout << "Unit of measure: centimeters" << endl;
                    meters = false;
                } else if (flag == "-maxsize") {
                    maxsize = atoi(vargs[++i]);
                } else if (flag == "-t") {
                    // .pdp file name
                    pdp_filename = string(vargs[++i]);
                    cout << ".PDP file:\t" << pdp_filename << endl;
                    generateTree = true;
                } else if (flag == "-m") {
                    // .ter file name
                    ter_filename = string(vargs[++i]);
                    grass_filename = string(vargs[++i]);
                    cout    << ".TER file:\t" << ter_filename << endl
                            << ".GRASS file:\t" << grass_filename << endl
                            << "Generating height + grass map only" << endl;
                    generateMap = true;
                } else if (flag == "-b") {
                    blur = true;
                    cout 	<< "Blur applied to grass map" << endl;
                } else if (flag == "-clustersize") {
                    minPlantsRequired = stoi(vargs[++i]);
                    cout    << "Minimum plants required: "
                            << minPlantsRequired << endl;
                } else if (flag == "-clusterstep") {
                	clusterStep = stoi(vargs[++i]);
                	cout 	<< "Cluster step: "
                			<< clusterStep << endl;
                }
            }
        

        // Display maximum coodinate sizeWWW
        cout << "Maxsize: " << maxsize << endl;

        // Verify input
        if ((ter_filename.empty() && pdp_filename.empty()) || filename_out.empty()) {
            cout << "ERR: Incorrect parameters given" << endl;
            return 0;
        }

        // Scale maxsize
        if (!meters)
            maxsize /= 100;

        // Create QuadTree
        AABB aabb(Point(float(width/2),float(width/2)), Point(width/2,width/2));
        Quadtree<Plant> qt(aabb);

        // Load in the .pdp file
        cout << "--------- STARTING ---------" << endl;
        if (generateTree) {
            cout << " * Generating Quadtree *" << endl;
            bool balanced = false;
            Quadtree<Plant> * QuadtreeOut = &qt;
 
            // Read plant details from the .pdp file
            if (readPDP(pdp_filename, width, qt, plants)) {
                balanced = isBalanced(&qt);
                // Generate balanced quadtree
                cout    << "Successfully generated QuadTree" << endl
                        << "\tNumber of plants processed: " << plantCount << endl
                        << "\tTree depth: " << getDepth(qt) << endl
                        << "\tMax X: " << maxX << " Max Y: " << maxY << endl
                        << "\tIs balanced? " << (balanced ? "True" : "False") << endl;
                height = 0;
            }

            // Work on clustering of plants
            cluster(&qt, width, 4, minPlantsRequired, clusterStep);

            // Save out Quadtree
            if (writePPDP(tree_filename, QuadtreeOut)) {
                cout    << "Successfully saved QuadTree to '" << filename_out << "'" << endl
                        << "\tNumber of nodes: " << nodeCount << endl
                        << "\tNumber of plants saved: " << plantSaveCount << endl
                        << "\tNumber of clustered plants saved: " << clusteredPlantSaveCount << endl
                        << "\tTree depth: " << getDepth(*QuadtreeOut) << endl;
                        if (!balanced)
                            cout << "\tIs balanced? " << (isBalanced(QuadtreeOut) ? "True" : "False") << endl;
            }
        }
        if (generateMap) {
            cout << " * Storing Height Map * " << endl;

            Map     *map = new Map();

            // Read in and store the altitude map
            readTerragen(ter_filename, *map);

            cout << " * Storing Grass Map * " << endl;
            // Read grass map
            readGrass(grass_filename, *map);
            if (blur) blurGrass(*map);

            // Save the map out
            if (map->saveFile(map_filename))
                cout << "Successfully saved Map to '" << map_filename << "'" << endl;

            delete map;
        }

        cout << "--------- FINISHED ---------" << endl;
        /*
        AABB rootAABB(Point(float(width/2),float(width/2)), Point(width/2,width/2));
        Quadtree<Plant> root(rootAABB, &qt);
        if (!balanced) {
            // Balance the tree
            cout << "--------- BALANCING ---------" << endl;

            for (int i = 0; i < plants.size(); i++) {
                root.insert(plants.at(i));
            }

            cout << "Successfully finished balancing quadtree" << endl;
            QuadtreeOut = &root;
        } else {
            QuadtreeOut = &qt;
        }*/

    } else cout << "Usage: \n\t- Parser.exe <file OUT prefix> <flags>" << endl
                << "\t Flags: (*required*)" << endl
                << "\t\t -t <.pdp file IN> : generate tree file only" << endl
                << " \t\t AND/OR" << endl
                << "\t\t -m <.ter file IN> <.grass file IN> : generate map file only" << endl
                << "\t Flags: (not required)" << endl
                << "\t\t -maxsize <value>: maximum coordinate size (default = 1 000 000)" << endl
                << "\t\t -cm : set unit size to centimeters (default)" << endl
                << "\t\t -clustersize <value>: number of minimum required plants to form a cluster (Default 10)" << endl
                << "\t\t -clusterstep <value>: iteration step through all plants during clustering (Default 1)" << endl
                << "\t\t -meters : set unit size to meters" << endl;

	return 0;
}