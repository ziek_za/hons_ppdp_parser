#ifndef MAP_H
#define MAP_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

class Map {
private:
	float 	step;
	int 	width, length, height;
	float	**data;
	

public:
	float	**grass;
	bool saveFile(string filename) {
		stringstream    ss;
	    FILE            *file;

	    if (height <= 0) { // Find the height by gettin the maximum from the map
	    	for (int i = 0; i < width; i++) {
	    		for (int k = 0; k < length; k++) {
	    			if (data[i][k] > height) height = data[i][k];
	    		}
	    	}
	    }

	    // Write header file
	    ss << width << " " << length << " " << height << " ";

	    // Write file content (altitude data)
	    for (int i = 0; i < width; i++) {
	    	for (int k = 0; k < length; k++) {
	    		ss << (height + data[i][k]) / (2 * height) << " ";
	    	}
	    }

	    ss << endl;

	    // Write out grass values
	    for (int i = 0; i < width; i++) {
	    	for (int k = 0; k < length; k++) {
	    		ss << grass[i][k] << " ";
	    	}
	    }
	    
	    // Convert to char array    
	    string s_out = ss.str();
	    
	    // Get the size
	    ss.seekg(0, ios::end);
	    int size = ss.tellg();
	    ss.seekg(0, ios::beg);
	    
	    try {
	        // Open file abd save as binary
	        file = fopen(&filename[0], "w");
	        // Write to the file
	        fwrite(s_out.c_str(), 1, size, file);
	    } catch (exception & e) {
	        cout << "ERR: " << e.what() << endl;
	        return false;
	    }

	    fclose(file);

	    return true;
	}

	void setStep(float newStep) { step = newStep;}

	void allocateGrass(int w, int h) {
		grass = new float* [w];

		for(int i = 0; i < w; ++i) {
		    grass[i] = new float[h];
		}
	}

	void allocate(int w, int l, int h) {
		height = h;
		setDimensions(w, l, h);
		data 	= new float* [w];

		for(int i = 0; i < w; ++i) {
		    data[i] = new float[l];
		}
	}

	void setValue(int x, int y, float h) {
		data[x][y] = h;
	}

	void getDimensions(int & w, int & l, int & h) {
		w = width;
		l = length;
		h = height;
	}

	void setDimensions(int w, int l, int h) {
		width = w;
		length = l;
		height = h;
	}

	float getValue(int x, int y){
		return data[x][y];
	}

	~Map(){
		for(int i = 0; i < width; ++i) {
		    delete [] data[i];
		    delete [] grass[i];
		}
		delete [] data;
		delete [] grass;
	}
};

#endif
