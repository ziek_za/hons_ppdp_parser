#include <iostream>
#include <fstream>
#include <algorithm>
#include <limits>
#include <cstring>
#include <utility>
#include <stdexcept>
#include <locale>
#include <cstdint>
#include <cmath>
#include <typeinfo>
#include <sstream>
#include <iomanip>
#include <stdio.h>

#include "common/debug_string.h"

using namespace std;

static void readGrass(const uts::string &filename, Map & ans) {
	ifstream file;
	file.open(filename);
	int width, height;

	if (file.is_open()) {
		file >> width >> height;
		ans.allocateGrass(width, height);

		for (int i = 0; i < width; i++) {
			for (int k = 0; k < height; k++) {
				file >> ans.grass[i][k];
			}
		}
	}

	file.close();
}

static void blurGrass(Map & ans) {
	int width, length, height;
	ans.getDimensions(width, length, height);

	// Generate a copy of the grass map to sample from
	float ** resultsMap = new float * [width];

	for (int i = 0; i < width; i++) {
		resultsMap[i] = new float[length];
		for (int k = 0; k < length; k++) {
			float val = 0.0f;
			int count = 0;
			for (int x = -1; x < 1; x++) {
				for (int y = -1; y < 1; y++) {
					if (i + x >= 0 && i + x < width &&
						k + y >= 0 && k + y < length) {
						val += ans.grass[i + x][k + y];
						count++;
					}
				}
			}
			resultsMap[i][k] = val / max(count,1);
		}
	}

	// Clear up space
	for(int i = 0; i < width; ++i) {
	    delete [] ans.grass[i];
	}
	delete [] ans.grass;

	// Set the maps grass map to the results map
	ans.grass = resultsMap;
}