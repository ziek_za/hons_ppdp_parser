#include <iostream>
#include <fstream>
#include <algorithm>
#include <limits>
#include <cstring>
#include <utility>
#include <stdexcept>
#include <locale>
#include <cstdint>
#include <cmath>
#include <typeinfo>
#include <sstream>
#include <iomanip>
#include <stdio.h>

#include "common/debug_string.h"
#include "map.h"

using namespace std;

/// Reads a binary little-endian float
static float readFloat(std::istream &in) {
	static_assert(std::numeric_limits<float>::is_iec559 && sizeof(float) == 4, "float is not IEEE-754 compliant");
	unsigned char data[4];
	std::uint32_t host = 0;
	float out;

	in.read(reinterpret_cast<char *>(data), 4);
	for (int i = 0; i < 4; i++)
		host |= std::uint32_t(data[i]) << (i * 8);
	std::memcpy(&out, &host, sizeof(out));
	return out;
}

/// Writes a binary little-endian float
static void writeFloat(std::ostream &out, float f) {
	static_assert(std::numeric_limits<float>::is_iec559 && sizeof(float) == 4, "float is not IEEE-754 compliant");
	std::uint32_t host;
	unsigned char data[4];

	std::memcpy(&host, &f, sizeof(host));
	for (int i = 0; i < 4; i++)
		data[i] = (host >> (i * 8)) & 0xff;
	out.write(reinterpret_cast<char *>(data), 4);
}

/// Reads a binary little-endian 16-bit unsigned int
static std::uint16_t readUint16(std::istream &in) {
	unsigned char data[2];
	in.read(reinterpret_cast<char *>(data), 2);
	return data[0] | (std::uint16_t(data[1]) << 8);
}

/// Writes a binary little-endian 16-bit unsigned int
static void writeUint16(std::ostream &out, std::uint16_t v) {
	unsigned char data[2];
	data[0] = v & 0xff;
	data[1] = v >> 8;
	out.write(reinterpret_cast<char *>(data), 2);
}

/// Reads a binary little-endian 16-bit signed int
static std::int16_t readInt16(std::istream &in) {
	return static_cast<std::int16_t>(readUint16(in));
}

/// Writes a binary little-endian 16-bit signed int
static void writeInt16(std::ostream &out, std::int16_t v) {
	writeUint16(out, static_cast<std::uint16_t>(v));
}

static void readTerragen(const uts::string &filename, Map & ans) {
	std::ifstream in(filename, std::ios::binary);
	if (!in)
		throw std::runtime_error("Could not open " + filename);

	//map ans;
	try {
		in.imbue(std::locale::classic());
		in.exceptions(std::ios::failbit | std::ios::badbit | std::ios::eofbit);
		char signature[16];
		in.read(signature, 16);
		if (uts::string(signature, 16) != "TERRAGENTERRAIN ") {
			throw std::runtime_error("signature did not match");
		}

		int width = -1;
		int height = -1;
		float step = 0.0f;
		while (true) {
			// Markers are aligned to 4-byte boundaries
			auto pos = in.tellg();
			if (pos & 3)
				in.seekg(4 - (pos & 3), std::ios::cur);

			char markerData[4];
			in.read(markerData, 4);
			uts::string marker(markerData, 4);
			if (marker == "XPTS")
				width = readUint16(in);
			else if (marker == "YPTS")
				height = readUint16(in);
			else if (marker == "SIZE")
				width = height = readUint16(in) + 1;
			else if (marker == "SCAL") {
				float stepX = readFloat(in);
				float stepY = readFloat(in);
				float stepZ = readFloat(in);
				if (stepY != stepX || stepZ != stepX)
					throw std::runtime_error("SCAL values are not all equal");
				else if (stepX <= 0.0f)
					throw std::runtime_error("SCAL value is negative");
				step = stepX;
			} else if (marker == "CRAD")
				readFloat(in); // radius of planet
			else if (marker == "CRVM")
				in.ignore(4);
			else if (marker == "ALTW") {
				if (step == 0.0f) {
					std::cerr
							<< "Warning: no scale found. Using spec default of 30\n";
					step = 30.0f;
				}
				float heightScale = readInt16(in) / 65536.0f;
				float baseHeight = readInt16(in);
				if (width <= 0 || height <= 0)
					throw std::runtime_error("ALTW found before dimensions");

				ans.setStep(step);
				ans.allocate(width, height, baseHeight);
				for (int y = 0; y < height; y++)
					for (int x = 0; x < width; x++) {
						float h = readInt16(in);
						ans.setValue(y, x, h * heightScale);
					}
			} else if (marker == "EOF ")
				break;
			else
				throw std::runtime_error("unexpected chunk `" + marker + "'");
		}
	} catch (std::runtime_error &e) {
		throw std::runtime_error(
				"Failed to read " + filename + ": " + e.what());
	}
	//return ans;
}

static void readAuxData(const uts::string &filename, Map & ans, int w, int h) {
	std::ifstream in(filename);
	if (!in)
		throw std::runtime_error("Could not open " + filename);
	try {
		std::string line;
		std::getline(in, line);
		std::stringstream header(line);
		std::string width, height;

		header >> width;
		header >> height;

		if (!((stoi(width) == w) && (stoi(height) == h)))
			throw std::runtime_error("Dimensions do not match");

		std::getline(in, line);
		std::stringstream data(line);
		std::string token;

		ans.allocate(w, h, 0);
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++) {
				data >> token;
				float val = stof(token);
				ans.setValue(x, y, val);
			}

	} catch (std::runtime_error &e) {
		throw std::runtime_error(
				"Failed to read " + filename + ": " + e.what());
	}

}
